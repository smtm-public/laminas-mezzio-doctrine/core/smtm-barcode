<?php

declare(strict_types=1);

namespace Smtm\Barcode\Infrastructure\Service;

use Smtm\Base\Infrastructure\Service\AbstractInfrastructureService;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class BarcodeService extends AbstractInfrastructureService
{
    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        protected array $config
    ) {
        parent::__construct($infrastructureServicePluginManager);
    }

    public function generateEan13Svg(string $code, array $options = []): array
    {
        $barcode = (new \Laminas\Barcode\Object\Ean13(
            $options
        ))->setText($code);
        $renderer = new \Laminas\Barcode\Renderer\Svg([
            'barcode' => $barcode,
        ]);
        $output = $renderer->draw();

        return [
            'data' => $output->saveXML(),
            'width' => $barcode->getWidth(),
            'height' => $barcode->getHeight(),
        ];
    }
}
