<?php

declare(strict_types=1);

namespace Smtm\Barcode\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
trait BarcodeServiceAwareTrait
{
    protected BarcodeService $barcodeService;

    public function getBarcodeService(): BarcodeService
    {
        return $this->barcodeService;
    }

    public function setBarcodeService(BarcodeService $barcodeService): static
    {
        $this->barcodeService = $barcodeService;

        return $this;
    }
}
