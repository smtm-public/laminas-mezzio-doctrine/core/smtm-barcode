<?php

declare(strict_types=1);

namespace Smtm\Barcode\Infrastructure\Service;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
interface BarcodeServiceAwareInterface
{
    public function getBarcodeService(): BarcodeService;
    public function setBarcodeService(BarcodeService $barcodeService): static;
}
