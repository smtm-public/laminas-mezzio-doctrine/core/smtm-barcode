<?php

declare(strict_types=1);

namespace Smtm\Barcode\Infrastructure\Service\Factory;

use Smtm\Barcode\Infrastructure\Service\BarcodeService;
use Smtm\Barcode\Infrastructure\Service\BarcodeServiceAwareInterface;
use Smtm\Base\Factory\ServiceNameAwareInterface;
use Smtm\Base\Factory\ServiceNameAwareTrait;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class BarcodeServiceAwareDelegator implements DelegatorFactoryInterface, ServiceNameAwareInterface
{

    use ServiceNameAwareTrait;

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var BarcodeServiceAwareInterface $object */
        $object = $callback();

        $object->setBarcodeService(
            $container
                ->get(InfrastructureServicePluginManager::class)
                ->get($options['name'] ?? $this->getServiceName() ?? BarcodeService::class)
        );

        return $object;
    }
}
