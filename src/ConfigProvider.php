<?php

declare(strict_types=1);

namespace Smtm\Barcode;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape([
        'dependencies' => 'array',
        'barcode' => 'array'
    ])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'barcode' => include __DIR__ . '/../config/barcode.php',
        ];
    }
}
