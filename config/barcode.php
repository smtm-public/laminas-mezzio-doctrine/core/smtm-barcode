<?php

declare(strict_types=1);

namespace Smtm\Barcode;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-barcode')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-barcode'
    );
    $dotenv->load();
}

//$barcodeStuff = json_decode(
//    EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_BARCODE_STUFF'),
//    true,
//    flags: JSON_THROW_ON_ERROR
//);

return [
//    'barcodeStuff' => json_decode(
//        EnvHelper::getEnvFromProcessOrSuperGlobal('SMTM_PDF_STUFF'),
//        true
//    ),
//    'remoteWkHtmlToPdfServices' => $remoteWkHtmlToPdfServices,
//    'fontsDir' => $fontsDir,
];
