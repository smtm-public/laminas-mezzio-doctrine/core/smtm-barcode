<?php

declare(strict_types=1);

namespace Smtm\Barcode;

use Smtm\Barcode\Infrastructure\Service\BarcodeService;
use Smtm\Barcode\Infrastructure\Service\Factory\BarcodeServiceFactory;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        InfrastructureServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $callback();

                return $infrastructureServicePluginManager->configure(
                    [
                        'factories' => [
                            BarcodeService::class => BarcodeServiceFactory::class,
                        ],
                    ]
                );
            }
        ],
    ],
];
